import React, {Component} from "react";
import './Header.css';

class Header extends Component{
    render() {
        return (
          <div>
              <h1 className="display-4 text-center">
                <i className="fas fa-book-open text-primary"></i>
                <span className="text-secondary">Book</span> List </h1>
              <p className="text-center">Add your book information to store it in database.</p>
          </div>
    );
    }
}

export default Header