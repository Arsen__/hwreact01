import React from "react";
import './BookChange.css';

const BookChange = (props) => {
    const {title, author, isbn, handleChange, handleEditChange} = props;
    return(
        <tr>
            <td>
                <div className="form-group">
                    <input type="text" name="title" className="form-control" placeholder={title} onChange={handleChange}/>
                </div>
            </td>
            <td>
                <div className="form-group">
                    <input type="text" name="author" className="form-control" placeholder={author} onChange={handleChange}/>
                </div>
            </td>
            <td>
                <div className="form-group">
                    <input type="text" name="isbn" className="form-control" placeholder={isbn} onChange={handleChange}/>
                </div>
            </td>
            <td>
                <input type="button" value="Update" className="btn btn-primary" onClick={handleEditChange}/>
            </td>
        </tr>
    )
}

export default BookChange