import React from 'react';
import './BookItem.css';

const BookItem = (props) => {
    const {title, author, isbn, handleEdit, handleRemove} = props;
    return (
        <tr data-id="12">
            <td>{title}</td>
            <td>{author}</td>
            <td>{isbn}</td>
            <td>
                <a href="#" className="btn btn-info btn-sm btn-edit" onClick={handleEdit}>
                    <i className="fas fa-edit"></i>
                </a>
            </td>
            <td>
                <a href="#" className="btn btn-danger btn-sm btn-delete" onClick={handleRemove}>X</a>
            </td>
        </tr>
    )
};

export default BookItem;