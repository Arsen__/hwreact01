import React, {Component} from 'react';
import './Book.css'
import FormGroup from '../../shared/FormGroup/Form-group';
import ConfirmButton from '../../shared/ConfirmButton/ConfirmButton';
import Total from '../Total/Total';
import BookItem from './BookItem/BookItem';
import BookChange from './BookChange/BookChange';


class Book extends Component{
    state = {
        title: '',
        author: '',
        isbn: '',
        allowAdd: false,
        forbidAdd: false,
        allowChange: false,
        valueText: undefined,
        allowCount: false,
        placeholderText: '',
        count: 0,
    };

    handleSubmit = () => {
        const {title, author, isbn, count} = this.state;
        const newCount = count + 1;
        if (title.length > 0 && author.length > 0 && isbn.length > 0){
            this.setState({
                valueText: ' ',
                allowAdd: true,
                forbidAdd: false,
                count: newCount,
                allowCount: true,
            })
            console.log(this.state)
        } if (title.length === 0 || author.length === 0 || isbn.length === 0) {
            this.setState({
                placeholderText: 'это поле обязательно к заполнению',
                allowAdd: false,
                forbidAdd: true,
            })
        }
    }

    handleAdd = ({target}) => {
        this.setState({
            [target.name]: target.value
        })
    }

    handleEdit = () => {
        this.setState({
            allowChange: true,
            allowAdd: false,
            }
        )
    }

    handleEditChange = () => {
        this.setState({
            allowAdd: true,
            allowChange: false
        })
    }



    handleRemove = () => {
        const {count} = this.state;
        const newCount = count - 1;
        this.setState({
            title: '',
            author: '',
            isbn: '',
            allowAdd: false,
            forbidAdd: false,
            allowChange: false,
            valueText: undefined,
            placeholderText: '',
            count: newCount,
            }
        )
    }

    render() {
        const {title, author, isbn, valueText, placeholderText, count} = this.state;
        const {handleSubmit, handleAdd, handleEdit, handleEditChange, handleRemove} = this;
        return (
            <div className='row'>
                <div className='col-lg-4'>
                    <form>
                        <FormGroup name={'title'} type={'text'} titleText={'Title'} handleChange={handleAdd} value={valueText} placeholder={placeholderText}/>
                        <FormGroup name={'author'} type={'text'} titleText={'Author'} handleChange={handleAdd} value={valueText} placeholder={placeholderText}/>
                        <FormGroup name={'isbn'} type={'text'} titleText={'ISBN#'} handleChange={handleAdd} value={valueText} placeholder={placeholderText}/>
                        <ConfirmButton type={'button'} value={'Confirm'} placeholder={'Confirm'} className={'btn btn-primary'} onSubmitItem={() => handleSubmit()}/>
                    </form>
                <h3 id='book-count' className='book-count mt-5'>Всего книг {this.state.allowCount? count : 0}</h3>
                </div>
                <table className='table table-striped mt-2'>
                    <Total/>
                    <tbody>
                        {
                            this.state.allowAdd?
                                <BookItem
                                    title={title}
                                    author={author}
                                    isbn={isbn} h
                                    handleEdit={() => handleEdit()}
                                    handleRemove={() => handleRemove()}/>
                                    :
                                <tr/>
                        }
                        {
                            this.state.allowChange?
                                <BookChange
                                    title={title}
                                    author={author}
                                    isbn={isbn}
                                    handleChange={handleAdd}
                                    handleEditChange={() => handleEditChange()}/>
                                    :
                                <tr/>
                        }
                    </tbody>
                </table>
            </div>
        )
        }
    }

export default Book;