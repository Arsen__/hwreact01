import React from "react";
import './Form-group.css';

const FormGroup = (props) => {
    const {type, titleText, handleChange, name, placeholder, value} = props;
    return (
        <div className='form-group'>
            <label>{titleText}</label>
            <input name={name} type={type} onChange={handleChange} placeholder={placeholder} value={value}/>
        </div>
    );
}

export default FormGroup