import React from "react";
import './ConfirmButton.css';

const ConfirmButton = (props) => {
    const {type, value, placeholder, className, onSubmitItem} = props;
    return(
        <div>
            <input type={type} value={value} placeholder={placeholder} className={className} onClick={onSubmitItem} />
        </div>
    )
}

export default ConfirmButton