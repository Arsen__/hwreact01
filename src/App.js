import React from 'react';
import './App.css';
import './bootstrap.min.css'
import Header from "./client/components/Header";
import Book from "./client/components/Book";

function App() {
  return (
      <div className="container mt-4">
        <Header />
        <Book />
      </div>
      );
}

export default App;
